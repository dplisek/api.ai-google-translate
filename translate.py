import requests
from six.moves.html_parser import HTMLParser

h = HTMLParser()


def translate(text):
    payload = {
        'q': text,
        'source': 'cs',
        'target': 'en'
    }
    response = requests\
        .post('https://translation.googleapis.com/language/translate/v2?key=AIzaSyBHGunSAipv-RjxnGaxO9fh5aukar3qRJc',
              json=payload)
    if response.status_code != 200:
        return None
    json = response.json()
    return h.unescape(json['data']['translations'][0]['translatedText'])

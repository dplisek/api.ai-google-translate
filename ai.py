import apiai
from flask import session
import uuid
import json

ai = apiai.ApiAI('0b7986734c8b44ecaf138488f299bd6a')


class AIReply:
    def __init__(self):
        pass

    messages = []
    intent = ''
    contexts = []


def get_reply(message):
    request = ai.text_request()
    request.lang = 'en'  # optional, default value equal 'en'
    request.session_id = get_session_id()
    request.query = message
    http_response = request.getresponse()
    if http_response.status != 200:
        return None
    response = json.loads(http_response.read().decode('utf-8'))
    reply = AIReply()
    reply.messages = []
    reply.messages.append(response['result']['fulfillment']['speech'])
    reply.intent = response['result']['metadata']['intentName']
    reply.contexts = []
    for context in response['result']['contexts']:
        reply.contexts.append(context['name'])
    return reply


def get_session_id():
    session_id = session.get('session_id', None)
    if not session_id:
        session_id = str(uuid.uuid4())
        session['session_id'] = session_id
    return session_id

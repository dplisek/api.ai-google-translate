from flask import Flask, render_template, redirect, url_for, session, request
from translate import translate
from ai import get_reply

app = Flask(__name__)


@app.route('/')
def main():
    conversation = session.get('conversation', [])
    return render_template('main.html', conversation=conversation)


@app.route('/send', methods=['POST'])
def send_message():
    conversation_step = {}
    message = request.form['message']
    conversation_step['user_message'] = message
    translated = translate(message)
    if not translated:
        conversation_step['translation'] = 'Couldn\'t translate.'
    else:
        conversation_step['translation'] = translated
        reply = get_reply(translated)
        if not reply:
            conversation_step['ai_messages'] = ['Couldn\'t get response.']
            conversation_step['ai_intent'] = 'Couldn\'t get response.'
            conversation_step['ai_contexts'] = ['Couldn\'t get response.']
        else:
            conversation_step['ai_messages'] = reply.messages
            conversation_step['ai_intent'] = reply.intent
            conversation_step['ai_contexts'] = reply.contexts
    conversation = session.get('conversation', [])
    conversation.insert(0, conversation_step)
    session['conversation'] = conversation
    return redirect(url_for('main'))


@app.route('/reset')
def reset():
    session.clear()
    return redirect(url_for('main'))


if __name__ == '__main__':
    app.secret_key = 'j3k54jkj5-45jk2.45k'
    app.run(host='0.0.0.0')
